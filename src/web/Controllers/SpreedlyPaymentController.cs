﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using AuthorizeNet.APICore;
using EPiServer;
using EPiServer.Core;
using EPiServer.Editor;
using EPiServer.Logging;
using EPiServer.Web.Routing;
using Mediachase.Commerce;
using Mediachase.Commerce.Orders;
using Mediachase.Commerce.Orders.Managers;
using Mediachase.Commerce.Website.Helpers;
using OxxCommerceStarterKit.Core.Extensions;
using OxxCommerceStarterKit.Core.Objects.SharedViewModels;
using OxxCommerceStarterKit.Core.PaymentProviders;
using OxxCommerceStarterKit.Core.PaymentProviders.Spreedly;
using OxxCommerceStarterKit.Core.PaymentProviders.Payment;
using OxxCommerceStarterKit.Core.Repositories.Interfaces;
using OxxCommerceStarterKit.Web.Business;
using OxxCommerceStarterKit.Web.Business.Analytics;
using OxxCommerceStarterKit.Web.Business.Payment;
using OxxCommerceStarterKit.Web.Models.PageTypes;
using OxxCommerceStarterKit.Web.Models.PageTypes.Payment;
using OxxCommerceStarterKit.Web.Models.PageTypes.System;
using OxxCommerceStarterKit.Web.Models.ViewModels;
using OxxCommerceStarterKit.Web.Models.ViewModels.Payment;
using LineItem = OxxCommerceStarterKit.Core.Objects.LineItem;
using OxxCommerceStarterKit.Core.Services;
using OxxCommerceStarterKit.Core.Helpers;
using OxxCommerceStarterKit.Core.Models;

namespace OxxCommerceStarterKit.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class SpreedlyPaymentController : PaymentBaseController<SpreedlyPaymentPage>
    {
        private readonly IContentRepository _contentRepository;
        private readonly IOrderService _orderService;
        private readonly ISiteSettingsProvider _siteConfiguration;
        private readonly IPaymentCompleteHandler _paymentCompleteHandler;
        private readonly ICurrentMarket _currentMarket;
        private readonly ILogger _logger;
        private readonly IDibsPaymentProcessor _paymentProcessor;
        private readonly IIdentityProvider _identityProvider;
        private readonly IReceiptViewModelBuilder _receiptViewModelBuilder;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentRepository"></param>
        /// <param name="orderService"></param>
        /// <param name="paymentCompleteHandler"></param>
        /// <param name="siteConfiguration"></param>
        /// <param name="currentMarket"></param>
        /// <param name="logger"></param>
        public SpreedlyPaymentController(IIdentityProvider identityProvider,
            IContentRepository contentRepository,
            IDibsPaymentProcessor paymentProcessor, IReceiptViewModelBuilder receiptViewModelBuilder, ILogger logger)
        {
            _identityProvider = identityProvider;
            _contentRepository = contentRepository;
            _paymentProcessor = paymentProcessor;
            _receiptViewModelBuilder = receiptViewModelBuilder;
            _logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <returns></returns>
        public ActionResult Index(SpreedlyPaymentPage currentPage)
        {
            CartHelper ch = new CartHelper(Cart.DefaultName);

            if (ch.IsEmpty && !PageEditing.PageIsInEditMode)
            {
                return View("Error/_EmptyCartError");
            }

            var orderInfo = new OrderInfo()
            {
                // Dibs expect the order to be without decimals
                Amount = Convert.ToInt32(ch.Cart.Total * 100),
                Currency = ch.Cart.BillingCurrency,
                OrderId = ch.Cart.GeneratePredictableOrderNumber(),
                ExpandOrderInformation = true
            };

            Guid paymentMethod = Guid.Empty; // When not set (creating a page), this is null
            if (string.IsNullOrEmpty(currentPage.PaymentMethod) == false)
                paymentMethod = new Guid(currentPage.PaymentMethod);

            SpreedlyPaymentViewModel model = new SpreedlyPaymentViewModel(_contentRepository, currentPage, orderInfo, ch.Cart);

            // Get cart and track it
            //Api.CartController cartApiController = new Api.CartController();
            //cartApiController.Language = currentPage.LanguageBranch;
            //List<Core.Objects.LineItem> lineItems = cartApiController.GetItems(Cart.DefaultName);
            //TrackBeforePayment(lineItems);

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ProcessPayment(SpreedlyPaymentPage currentPage, string Token)
        {
            ReceiptPage receiptPage = new ReceiptPage();
            SpreedlyPaymentResult result = new SpreedlyPaymentResult();

            if (_log.IsDebugEnabled())
                _log.Debug("Payment processed: {0}", result);

            CartHelper cartHelper = new CartHelper(Cart.DefaultName);
            string message = "";
            OrderViewModel orderViewModel = null;

            if (cartHelper.Cart.OrderForms.Count > 0)
            {

                // Temporary Simulation of dummy card payment

                // Retain Payment gateway
                APIFeedback res1 = SpreedlyAPIHelper.RetainPaymentGateway(SiteConstants.SpreedlyGatewayTokenField.AppSetting());

                // Retain Payment method
                APIFeedback res2 = SpreedlyAPIHelper.RetainPaymentMethod(Token); // "BtD2K6zykoiwWtgeTeo3L2B97il");

                // Verify a payment gateway and method
                APIFeedback res3 = SpreedlyAPIHelper.VerifyPaymentMethod(SiteConstants.SpreedlyGatewayTokenField.AppSetting(), Token);

                // Authorize a payment
                APIFeedback res4 = SpreedlyAPIHelper.AuthorizePayment(SiteConstants.SpreedlyGatewayTokenField.AppSetting(), Token, cartHelper.Cart.Total);

                // Capture a partial payment
                APIFeedback res5 = null;
                if (res4 != null && !string.IsNullOrWhiteSpace(res4.TransactionToken))
                    res5 = SpreedlyAPIHelper.CapturePayment(res4.TransactionToken, cartHelper.Cart.Total / 2);

                // Refund credit partial amount
                APIFeedback res6 = null;
                if (res4 != null && !string.IsNullOrWhiteSpace(res4.TransactionToken))
                    res6 = SpreedlyAPIHelper.RefundPayment(res4.TransactionToken, cartHelper.Cart.Total / 2);

                /////////////////////////////////////////////

                var payment = cartHelper.Cart.OrderForms[0].Payments[0] as Core.PaymentProviders.Payment.SpreedlyPayment;

                if (payment != null)
                {
                    payment.TransactionID = result.TransactionToken;
                    payment.AuthorizationCode = result.TransactionToken;
                    payment[SiteConstants.PaymentAuthorizationTokenKey] = result.TransactionToken;
                    payment.TransactionType = TransactionType.Authorization.ToString();
                    payment.Status = result.Status;
                    cartHelper.Cart.Status = SiteConstants.SpreedlyPaymentCompleted;
                }
                else
                {
                    throw new Exception("Not a Spreedly Payment");
                }

                var orderNumber = cartHelper.Cart.GeneratePredictableOrderNumber();

                _log.Debug("Order placed - order number: " + orderNumber);

                cartHelper.Cart.OrderNumberMethod = CartExtensions.GeneratePredictableOrderNumber;

                var results = OrderGroupWorkflowManager.RunWorkflow(cartHelper.Cart, OrderGroupWorkflowManager.CartCheckOutWorkflowName);
                message = string.Join(", ", OrderGroupWorkflowManager.GetWarningsFromWorkflowResult(results));

                if (message.Length == 0)
                {
                    cartHelper.Cart.SaveAsPurchaseOrder();
                    cartHelper.Cart.Delete();
                    cartHelper.Cart.AcceptChanges();
                }

                var order = _orderService.GetOrderByTrackingNumber(orderNumber);

                // Must be run after order is complete, 
                // This will release the order for shipment and 
                // send the order receipt by email
                _paymentCompleteHandler.ProcessCompletedPayment(order, User.Identity);

                orderViewModel = new OrderViewModel(_currentMarket.GetCurrentMarket().DefaultCurrency.Format, order);
            }

            ReceiptViewModel model = new ReceiptViewModel(receiptPage);
            model.CheckoutMessage = message;
            model.Order = orderViewModel;

            // Track successfull order in Google Analytics
            TrackAfterPayment(model);

            return View("ReceiptPage", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void TrackAfterPayment(ReceiptViewModel model)
        {
            // Track Analytics 
            GoogleAnalyticsTracking tracking = new GoogleAnalyticsTracking(ControllerContext.HttpContext);

            // Add the products
            int i = 1;
            foreach (OrderLineViewModel orderLine in model.Order.OrderLines)
            {
                if (string.IsNullOrEmpty(orderLine.Code) == false)
                {
                    tracking.ProductAdd(code: orderLine.Code,
                        name: orderLine.Name,
                        quantity: orderLine.Quantity,
                        price: (double)orderLine.Price,
                        position: i
                        );
                    i++;
                }
            }

            // And the transaction itself
            tracking.Purchase(model.Order.OrderNumber,
                null, (double)model.Order.TotalAmount, (double)model.Order.Tax, (double)model.Order.Shipping);
        }
    }
}