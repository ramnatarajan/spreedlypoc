﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.Logging;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;
using Mediachase.Commerce;
using Mediachase.Commerce.Core;
using Mediachase.Commerce.Orders;
using Mediachase.Commerce.Orders.Dto;
using Mediachase.Commerce.Orders.Managers;
using Mediachase.Commerce.Website.Helpers;
using OxxCommerceStarterKit.Core.PaymentProviders;
using OxxCommerceStarterKit.Core.PaymentProviders.Spreedly;
using OxxCommerceStarterKit.Web.Business;
using OxxCommerceStarterKit.Web.Models.PageTypes;
using OxxCommerceStarterKit.Web.Models.PageTypes.Payment;
using OxxCommerceStarterKit.Core.Helpers;

namespace OxxCommerceStarterKit.Web.Models.ViewModels.Payment
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SpreedlyPaymentViewModel : GenericPaymentViewModel<SpreedlyPaymentPage>
    {
        private Cart _currentCart = null;
        private Mediachase.Commerce.Orders.Payment _payment;
        private PaymentMethodDto _paymentMethod;

        /// <summary>
        /// 
        /// </summary>
        private Cart CurrentCart
        {
            get
            {
                if (_currentCart == null)
                {
                    _currentCart = new CartHelper(Cart.DefaultName).Cart;
                }

                return _currentCart;
            }
            set { _currentCart = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentMethod"></param>
        /// <param name="currentPage"></param>
        /// <param name="orderInfo"></param>
        /// <param name="cart"></param>
        public SpreedlyPaymentViewModel(IContentRepository contentRepository, SpreedlyPaymentPage currentPage, OrderInfo orderInfo, Cart cart)
            : base(new Guid(currentPage.PaymentMethod), currentPage, orderInfo, cart)
        {
            PaymentMethodDto payment = PaymentManager.GetPaymentMethodBySystemName(SiteConstants.SpreedlySystemName, SiteContext.Current.LanguageName);
            _paymentMethod = payment;
            _currentCart = cart;

            SpreedlyPaymentGateway gw = new SpreedlyPaymentGateway();

            //orderInfo.Merchant = gw.Merchant;

            Mediachase.Commerce.Orders.Payment[] payments;
            if (CurrentCart != null && CurrentCart.OrderForms != null && CurrentCart.OrderForms.Count > 0)
            {
                payments = CurrentCart.OrderForms[0].Payments.ToArray();
            }
            else
            {
                payments = new Mediachase.Commerce.Orders.Payment[0];
            }
            _payment = payments.FirstOrDefault(c => c.PaymentMethodId.Equals(payment.PaymentMethod.Rows[0]["PaymentMethodId"]));


            OrderID = orderInfo.OrderId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentLink"></param>
        /// <returns></returns>
        private string GetViewUrl(ContentReference contentLink)
        {
            var url = UrlResolver.Current.GetUrl(contentLink, null, new VirtualPathArguments() {ContextMode = ContextMode.Default});
            return url;
        }
    }
}