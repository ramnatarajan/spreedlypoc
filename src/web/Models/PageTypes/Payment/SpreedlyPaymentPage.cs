﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using OxxCommerceStarterKit.Core.Attributes;

namespace OxxCommerceStarterKit.Web.Models.PageTypes.Payment
{
    /// <summary>
    /// 
    /// </summary>
    [ContentType(DisplayName = "Spreedly Payment Page",
        GUID = "b78efaa6-5a8b-4fc0-be2c-4c550fa1a29b",
        Description = "Use this page for Spreedly client side payment option spreedly express",
        AvailableInEditMode = true,
        GroupName = "Payment")]
    [SiteImageUrl]
    public class SpreedlyPaymentPage : BasePaymentPage
    {
        /// <summary>
        /// 
        /// </summary>
        [CultureSpecific]
        [Display(
            Name = "Main Body",
            Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual XhtmlString MainBody { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(
                    Name = "Cancel Page title",
                    Description = "Cancel Page ",
                    GroupName = "Cancel Page",
                    Order = 20)]
        [CultureSpecific]
        public virtual string CancelPageTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(
            Name = "Cancel Body",
            Description = "The content to show a user that has cancelled the payment",
            GroupName = "Cancel Page",
            Order = 40)]
        [CultureSpecific]
        public virtual XhtmlString CancelBodyText { get; set; }
    }
}