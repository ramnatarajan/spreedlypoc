﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxxCommerceStarterKit.Core.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class APIFeedback
    {
        /// <summary>
        /// 
        /// </summary>
        public string TransactionToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string GatewayToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PaymentMethodToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Succeeded { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TransactionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CurrencyCode { get; set; }
    }
}
