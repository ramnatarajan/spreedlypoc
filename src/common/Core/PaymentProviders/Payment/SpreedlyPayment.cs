﻿using Mediachase.Commerce.Orders;
using Mediachase.MetaDataPlus.Configurator;
using OxxCommerceStarterKit.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OxxCommerceStarterKit.Core.PaymentProviders.Payment
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class SpreedlyPayment : Mediachase.Commerce.Orders.Payment
    {
        /// <summary>
        /// 
        /// </summary>
        public static MetaClass SpreedlyPaymentMetaClass
        {
            get
            {
                if (SpreedlyPayment._MetaClass == null)
                    SpreedlyPayment._MetaClass = MetaClass.Load(OrderContext.MetaDataContext, "SpreedlyPayment");
                return SpreedlyPayment._MetaClass;
            }
        }

        public SpreedlyPayment()
            : base(SpreedlyPayment.SpreedlyPaymentMetaClass)
        {

        }

        public SpreedlyPayment(MetaClass metaClass)
            : base(metaClass)
        {
            this.PaymentType = PaymentType.CreditCard;

        }

        public SpreedlyPayment(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.PaymentType = PaymentType.Other;
        }

        private static MetaClass _MetaClass;

        /// <summary>
        /// 
        /// </summary>
        public string PaymentMethodToken
        {
            get { return base.GetString(SiteConstants.PaymentMethodTokenKey); }
            set { this[SiteConstants.PaymentMethodTokenKey] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string PaymentGatewayToken
        {
            get { return base.GetString(SiteConstants.PaymentGatewayTokenKey); }
            set { this[SiteConstants.PaymentGatewayTokenKey] = value; }
        }

        public string PaymentAuthorizationToken
        {
            get { return base.GetString(SiteConstants.PaymentAuthorizationTokenKey); }
            set { this[SiteConstants.PaymentAuthorizationTokenKey] = value; }
        }
    }
}
