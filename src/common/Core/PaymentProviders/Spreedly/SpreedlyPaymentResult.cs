﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxxCommerceStarterKit.Core.PaymentProviders.Spreedly
{
    /// <summary>
    /// 
    /// </summary>
    public class SpreedlyPaymentResult
    {
        public string AcceptReturnurl { get; set; }
        public string ActionCode { get; set; }
        public int Amount { get; set; }
        public string Orderid { get; set; }
        public string Status { get; set; }
        public string TransactionToken { get; set; }
        public string ValidationErrors { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Status: {0}, Action Code: {1}, Spreedly Token: {2}", Status, ActionCode, ValidationErrors);
        }
    }
}
