﻿using System;
using System.Net;
using System.Text;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.Logging;
using Mediachase.Commerce.Core;
using Mediachase.Commerce.Orders;
using Mediachase.Commerce.Orders.Dto;
using Mediachase.Commerce.Orders.Managers;
using Mediachase.Commerce.Plugins.Payment;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using OxxCommerceStarterKit.Core.Helpers;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using OxxCommerceStarterKit.Core.Models;

namespace OxxCommerceStarterKit.Core.PaymentProviders.Spreedly
{
    /// <summary>
    /// 
    /// </summary>
    public class SpreedlyPaymentGateway : AbstractPaymentGateway
    {
        private string _merchant;
        private PaymentMethodDto _payment;
        private static string _key;
        private ILogger _log = LogManager.GetLogger();

        /// <summary>
        /// Processes the payment.
        /// </summary>
        /// <param name="payment">The payment.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public override bool ProcessPayment(Mediachase.Commerce.Orders.Payment payment, ref string message)
        {
            if (payment != null && !string.IsNullOrWhiteSpace(payment.AuthorizationCode) && payment[SiteConstants.PaymentGatewayTokenKey] != null && !string.IsNullOrWhiteSpace(payment[SiteConstants.PaymentGatewayTokenKey].ToString()))
            {
                APIFeedback pRes = SpreedlyAPIHelper.AuthorizePayment(payment[SiteConstants.PaymentGatewayTokenKey].ToString(), payment.AuthorizationCode, payment.Amount);

                // TODO: update Payment object with more details and state of the transaction
                if (pRes != null && !string.IsNullOrWhiteSpace(pRes.TransactionToken))
                {
                    payment[SiteConstants.PaymentAuthorizationTokenKey] = pRes.TransactionToken;
                    payment.AuthorizationCode = pRes.TransactionToken;
                    payment.AcceptChanges();
                }

                return pRes != null && pRes.Succeeded;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        private string GetAmount(Mediachase.Commerce.Orders.Payment payment)
        {
            return payment != null ? (payment.Amount * 100).ToString("0") : string.Empty;
        }

        /// <summary>
        /// Gets the payment.
        /// </summary>
        /// <value>The payment.</value>
        public PaymentMethodDto Payment
        {
            get
            {
                if (_payment == null)
                    _payment = PaymentManager.GetPaymentMethodBySystemName(SiteConstants.SpreedlySystemName, SiteContext.Current.LanguageName);

                return _payment;
            }
        }

        /// <summary>
        /// Gets the merchant.
        /// </summary>
        /// <value>The merchant.</value>
        public string Merchant
        {
            get
            {
                if (String.IsNullOrEmpty(_merchant))
                    _merchant = GetParameterByName(Payment, SiteConstants.UserParameter).Value;

                return _merchant;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Key
        {
            get
            {
                if (string.IsNullOrEmpty(_key))
                    _key = GetParameterByName(Payment, SiteConstants.KeyParameter).Value;

                return _key;
            }
        }

        /// <summary>
        /// Gets the parameter by name.
        /// </summary>
        /// <param name="paymentMethodDto">The payment method dto.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static PaymentMethodDto.PaymentMethodParameterRow GetParameterByName(PaymentMethodDto paymentMethodDto, string name)
        {
            PaymentMethodDto.PaymentMethodParameterRow[] rowArray = (PaymentMethodDto.PaymentMethodParameterRow[])paymentMethodDto.PaymentMethodParameter.Select(string.Format("Parameter = '{0}'", name));

            if ((rowArray != null) && (rowArray.Length > 0))
                return rowArray[0];

            throw new ArgumentNullException("Parameter named " + name + " for Spreedly Authorization cannot be null");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public bool CaptureTransaction(Mediachase.Commerce.Orders.Payment payment)
        {
            if (payment != null && !string.IsNullOrWhiteSpace(payment.AuthorizationCode))
            {
                APIFeedback cRes = SpreedlyAPIHelper.CapturePayment(payment.AuthorizationCode);

                // TODO: update Payment object with more details and state of the transaction

                return cRes != null && cRes.Succeeded;
            }

            return false;
        }
    }
}
