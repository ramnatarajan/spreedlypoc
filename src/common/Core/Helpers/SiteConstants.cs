﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxxCommerceStarterKit.Core.Helpers
{
    public static class SiteConstants
    {
        public static readonly  string SessionCurrentOrderIdKey = "CurrentOrderID";
        public static readonly string SessionCurrentOrderNumberKey = "CurrentOrderNumber";
        public static readonly string SessionCurrentOrderTotalKey = "CurrentOrderTotal";
        public static readonly string UserParameter = "MerchantID";
        public static readonly string ProcessingUrl = "ProcessingUrl";
        public static readonly string KeyParameter = "Key";
        public static readonly string Capture = "CaptureTransaction";
        public static readonly string GBCurrencyCode = "GBP";

        #region Spreedly

        public static readonly string SessionSpreedlyAuthToken = "CurrentSpreedlyAuthToken";
        public static readonly string SpreedlySystemName = "Spreedly";
        public static readonly string SpreedlyPaymentCompleted = "Spreedly Authorization Completed";
        public static readonly string SpreedlyEnvironmentKeyField = "SpreedlyEnvironmentKey";
        public static readonly string SpreedlyAccessSecretField = "SpreedlyAccessSecret";
        public static readonly string SpreedlyAPIField = "SpreedlyAPI";
        public static readonly string SpreedlyGatewayTokenField = "SpreedlyGatewayToken";

        #endregion

        #region MetaField names

        public static readonly string PaymentGatewayTokenKey = "PaymentGatewayToken";
        public static readonly string PaymentMethodTokenKey = "PaymentMethodToken";
        public static readonly string PaymentAuthorizationTokenKey = "PaymentAuthorizationToken";

        #endregion
    }
}
