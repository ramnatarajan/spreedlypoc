﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxxCommerceStarterKit.Core.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class StaticHelper
    {
        /// <summary>
        /// Get the Application Settings
        /// </summary>
        /// <param name="key">Configuration Key</param>
        /// <returns>Key Value</returns>
        public static string AppSetting(this string key)
        {
            return ConfigurationManager.AppSettings[key] != null ? ConfigurationManager.AppSettings[key].ToString() : string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="InputString"></param>
        /// <returns></returns>
        public static string ExtractString(string InputString)
        {
            return !string.IsNullOrWhiteSpace(InputString) ? InputString : string.Empty;
        }
    }
}
