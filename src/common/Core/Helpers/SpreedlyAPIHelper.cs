﻿using System;
using System.Net;
using System.Text;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.Logging;
using Mediachase.Commerce.Core;
using Mediachase.Commerce.Orders;
using Mediachase.Commerce.Orders.Dto;
using Mediachase.Commerce.Orders.Managers;
using Mediachase.Commerce.Plugins.Payment;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using OxxCommerceStarterKit.Core.Helpers;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using Microsoft.CSharp;
using OxxCommerceStarterKit.Core.Models;

namespace OxxCommerceStarterKit.Core.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public enum RequestMethod
    {
        GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
    }

    /// <summary>
    /// 
    /// </summary>
    public enum APICallType
    {
        RETAIN, REDACT, AUTHORIZE, CAPTURE, CANCEL, REFUND, CREDIT, VERIFY
    }

    /// <summary>
    /// This instance holds all the calls made to spreedly payment API
    /// </summary>
    public static class SpreedlyAPIHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public static string APIPrefix
        {
            get
            {
                return SiteConstants.SpreedlyAPIField.AppSetting();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string APIAuthToken
        {
            get
            {
                return Convert.ToBase64String(Encoding.ASCII.GetBytes(SiteConstants.SpreedlyEnvironmentKeyField.AppSetting()
                    + ":" + SiteConstants.SpreedlyAccessSecretField.AppSetting()));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GatewayToken"></param>
        /// <returns></returns>
        public static APIFeedback RetainPaymentGateway(string GatewayToken)
        {
            if (!string.IsNullOrWhiteSpace(GatewayToken))
            {
                Task<string> callFeedback = MakeAPICall(string.Empty, "gateways/" + GatewayToken + "/retain.json", RequestMethod.PUT);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GatewayToken"></param>
        /// <returns></returns>
        public static APIFeedback RedactPaymentGateway(string GatewayToken)
        {
            if (!string.IsNullOrWhiteSpace(GatewayToken))
            {
                Task<string> callFeedback = MakeAPICall(string.Empty, "gateways/" + GatewayToken + "/redact.json", RequestMethod.PUT);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="MethodToken"></param>
        /// <returns></returns>
        public static APIFeedback RetainPaymentMethod(string MethodToken)
        {
            if (!string.IsNullOrWhiteSpace(MethodToken))
            {
                Task<string> callFeedback = MakeAPICall(string.Empty, "payment_methods/" + MethodToken + "/retain.json", RequestMethod.PUT);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="MethodToken"></param>
        /// <returns></returns>
        public static APIFeedback RedactPaymentMethod(string MethodToken)
        {
            if (!string.IsNullOrWhiteSpace(MethodToken))
            {
                Task<string> callFeedback = MakeAPICall(string.Empty, "payment_methods/" + MethodToken + "/redact.json", RequestMethod.PUT);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GatewayToken"></param>
        /// <param name="MethodToken"></param>
        /// <param name="CaptureAmount"></param>
        /// <returns></returns>
        public static APIFeedback AuthorizePayment(string GatewayToken, string MethodToken, decimal CaptureAmount)
        {
            if (!string.IsNullOrWhiteSpace(GatewayToken) && !string.IsNullOrWhiteSpace(MethodToken))
            {
                dynamic payload = new JObject();
                payload.transaction = new JObject();
                payload.transaction.payment_method_token = MethodToken;
                payload.transaction.amount = CaptureAmount;
                payload.transaction.currency_code = SiteConstants.GBCurrencyCode;
                Task<string> callFeedback = MakeAPICall(payload.ToString(), "gateways/" + GatewayToken + "/authorize.json", RequestMethod.POST);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TransactionToken"></param>
        /// <param name="CaptureAmount">Populate if it is a partial capture</param>
        /// <returns></returns>
        public static APIFeedback CapturePayment(string TransactionToken, decimal CaptureAmount = 0)
        {
            if (!string.IsNullOrWhiteSpace(TransactionToken))
            {
                dynamic payload = new JObject();
                payload.transaction = new JObject();
                payload.transaction.amount = CaptureAmount;
                payload.transaction.currency_code = SiteConstants.GBCurrencyCode;
                Task<string> callFeedback = MakeAPICall(CaptureAmount > 0 ? payload.ToString() : string.Empty, "transactions/" + TransactionToken + "/capture.json", RequestMethod.POST);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TransactionToken"></param>
        /// <returns></returns>
        public static APIFeedback CancelPayment(string TransactionToken)
        {
            if (!string.IsNullOrWhiteSpace(TransactionToken))
            {
                Task<string> callFeedback = MakeAPICall(string.Empty, "transactions/" + TransactionToken + "/void.json", RequestMethod.POST);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TransactionToken"></param>
        /// <param name="CreditAmount">Use for partial refund</param>
        /// <returns></returns>
        public static APIFeedback RefundPayment(string TransactionToken, decimal CreditAmount = 0)
        {
            if (!string.IsNullOrWhiteSpace(TransactionToken))
            {
                dynamic payload = new JObject();
                payload.transaction = new JObject();
                payload.transaction.amount = CreditAmount;
                payload.transaction.currency_code = SiteConstants.GBCurrencyCode;
                Task<string> callFeedback = MakeAPICall(CreditAmount > 0 ? payload.ToString() : string.Empty, "transactions/" + TransactionToken + "/credit.json", RequestMethod.POST);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GatewayToken"></param>
        /// <param name="MethodToken"></param>
        /// <param name="CreditAmount"></param>
        /// <returns></returns>
        public static APIFeedback GeneralCredit(string GatewayToken, string MethodToken, decimal CreditAmount)
        {
            if (!string.IsNullOrWhiteSpace(GatewayToken) && !string.IsNullOrWhiteSpace(MethodToken))
            {
                dynamic payload = new JObject();
                payload.transaction = new JObject();
                payload.transaction.payment_method_token = MethodToken;
                payload.transaction.amount = CreditAmount;
                payload.transaction.currency_code = SiteConstants.GBCurrencyCode;
                Task<string> callFeedback = MakeAPICall(payload.ToString(), "gateways/" + GatewayToken + "/general_credit.json", RequestMethod.POST);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GatewayToken"></param>
        /// <param name="MethodToken"></param>
        /// <returns></returns>
        public static APIFeedback VerifyPaymentMethod(string GatewayToken, string MethodToken)
        {
            if (!string.IsNullOrWhiteSpace(GatewayToken) && !string.IsNullOrWhiteSpace(MethodToken))
            {
                dynamic payload = new JObject();
                payload.transaction = new JObject();
                payload.transaction.payment_method_token = MethodToken;
                payload.transaction.retain_on_success = true;
                Task<string> callFeedback = MakeAPICall(payload.ToString(), "gateways/" + GatewayToken + "/verify.json", RequestMethod.POST);
                callFeedback.Wait();
                string resultData = callFeedback.Result.ToString();
                return GetAPIFeedback(resultData);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="JSONPayload"></param>
        /// <param name="ActionEndpoint"></param>
        /// <returns></returns>
        public static async Task<string> MakeAPICall(string JSONPayload, string ActionEndpoint, RequestMethod CurrentRequest)
        {
            if (!string.IsNullOrWhiteSpace(ActionEndpoint))
            {
                string ResultData = string.Empty;
                HttpResponseMessage response = null;
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", APIAuthToken);

                switch (CurrentRequest)
                {
                    case RequestMethod.GET:
                        response = await client.GetAsync(SiteConstants.SpreedlyAPIField.AppSetting() + ActionEndpoint).ConfigureAwait(continueOnCapturedContext: false);
                        break;
                    case RequestMethod.PUT:
                        response = await client.PutAsync(SiteConstants.SpreedlyAPIField.AppSetting() + ActionEndpoint,
                    new StringContent(JSONPayload, Encoding.UTF8, "application/json")).ConfigureAwait(continueOnCapturedContext: false);
                        break;
                    case RequestMethod.POST:
                        response = await client.PostAsync(SiteConstants.SpreedlyAPIField.AppSetting() + ActionEndpoint,
                    new StringContent(JSONPayload, Encoding.UTF8, "application/json")).ConfigureAwait(continueOnCapturedContext: false);
                        break;
                }

                ResultData = response.Content.ReadAsStringAsync().Result;
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                    return ResultData;
            }

            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ResultData"></param>
        /// <returns></returns>
        public static APIFeedback GetAPIFeedback(string ResultData)
        {
            if (!string.IsNullOrWhiteSpace(ResultData))
            {
                dynamic resultPayload = JObject.Parse(ResultData);

                if (resultPayload != null && resultPayload.transaction != null && resultPayload.transaction.succeeded == true)
                    return new APIFeedback
                    {
                        TransactionToken = resultPayload.transaction.token != null ? resultPayload.transaction.token.ToString() : string.Empty,
                        Succeeded = true,
                        TransactionType = resultPayload.transaction.transaction_type != null ? resultPayload.transaction.transaction_type.ToString() : string.Empty,
                        Message = resultPayload.transaction.message != null ? resultPayload.transaction.message.ToString() : string.Empty,
                        GatewayToken = resultPayload.transaction.gateway_token != null ? resultPayload.transaction.gateway_token.ToString() : resultPayload.transaction.gateway != null && resultPayload.transaction.gateway.token != null ? resultPayload.transaction.gateway.token.ToString() : string.Empty,
                        CurrencyCode = resultPayload.transaction.currency_code != null ? resultPayload.transaction.currency_code.ToString() : string.Empty,
                        PaymentMethodToken = resultPayload.transaction.payment_method != null && resultPayload.transaction.payment_method.token != null ? resultPayload.transaction.payment_method.token.ToString() : string.Empty
                    };
            }

            return null;
        }
    }
}
